# Demo of Layer Structure

### Junle Li <junlel@microsoft.com>

This repo demonstrates the basic ideas about layer structure. It is a way to organize the React components to be robust.

Change the `import` statement in `app/App.tsx` file to follow the post step by step. The feature of the control is written in the `App.tsx` file, on the page.

Before start the steps, change `App.tsx` to import from `titleImage_layerStructure` folder which is error-free to play with the control. We use `setTimeout` to simulate the image upload process. Change the timeout duration and check the corner cases in the *Hard Parts* section.

![Demo](./public/demo.jpg)

## Display State

> *HINT:* The implementation is inside `titleImage_displayState` folder.

I hope you have played with the control, let us start thinking about how to implement it.

From the traditional React lesson, we should split the control to different UI parts:

- `Image` - for the background image to display.
- `Cropper` - for the control to crop the image.
- `Spinner` - for the spinner during uploading.
- `Text` - for the text to input or display.
- `TitleImage` - to combine the above components as a control.

The first four components have been well defined and implemented in four different folders in this repo.

Now, let us think about the `TitleImage` component. It needs some React states to control the show/hide state of different controls.

The first idea is to have one React state for each show/hide state. We will have intuitive render logic, but complicated handle methods. Worse, we lost some important information when switching states.

## Model State

> *HINT:* The implementation is inside `titleImage_modelState` folder.

To resolve the dark side from the first solution, we can quickly come up to define the React state for the data model, not for the display components. So, we have the following React state definition.

- `mode` - either `edit` mode or `preview` mode.
- `isUploading` - indicates the background image is uploading.
- `isCropping` - indicates if show the image cropper control for corpping.
- `text` - the title text.
- `cropInfo` - the cropping information applied to background image.

Now, we have clear action handlers, but complicated render logics. Some components are rendered under a complicated situation.

However, it still don't have an easy way to handle the state change after the async action is finished. The state after image upload is finished depends on the display mode.

The deep issue is, we have some dependencies on the state. It is hard to sort out and maintain correctly the dependencies inside one React component.

## Layer Structure

> *HINT:* The implementation is inside `titleImage_layerStructure` folder.

Follow the previous idea, it is hard to maintain the dependent state inside one React component, why not split the component to different layers and each layer handles one part of state.

Let us review the dependency tree of the state, we can come up with:

![State tree](./public/state-tree.jpg)

After we draw the dependency tree, the next part is easy - to implement the React component layers following the exact diagram. Inside each branch, we can combine the primitive components for different UI combinations.

There are two types of React components - UI component handles the display UI elements, and (logical) layer components handle the logic flow about the control states. The state stores *inside* the React component tree, the code is the data.

Please note that, we are creating some React components map to no UI elements, they are created dedicated for state management. Because we leverage the React tree for logic computation, we have some down side of the solution.

- The `ref` of component is hard to pass through the whole React tree. A workaround is to leverage `controller` and `WeakMap`.
- After a logic is changed, the React tree structure may be completed changed even through there is a little UI elements are changed. The `mount` and `unmount` React life-cycle may be the right place for expensive operations.
- Because the React tree is following the logic flow instead of the UI structure. The pattern to *move up the shared states to upper component* will not work. The solution is to leverage aspect-oriented programming (AOP) to share states across components in different logic branches.

## Compare to Redux

> https://redux.js.org/

Redux *encourage* to use the flatten data structure to store the state. Sometimes, that won't work. The real data model is a tree instead of flatten, it is hard and buggy to simulate a tree with a flatten data structure.

## Compare to Mobx

> https://mobx.js.org/

Mobx does not stick to any state pattern. Its *transparently applying functional reactive programming* (TFRP) ability can build the data model as a tree. It means, Mobx provide the ability to build the logical tree with ES class, leave React to only handle the UI tree.
