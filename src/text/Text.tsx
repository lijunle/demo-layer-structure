import * as React from 'react';

import './Text.css';

export interface ITextProps {
  className?: string;
  preview: boolean;
  value: string;
  onChange(value: string): void;
}

export function Text(props: ITextProps): JSX.Element {
  function onChange(event: React.ChangeEvent<HTMLInputElement>): void {
    props.onChange(event.target.value);
  }

  return props.preview
    ? (
      <p
        className={`Text-title ${props.className}`}
      >
        {props.value}
      </p>
    )
    : (
      <input
        className={`Text-title Text-title--edit ${props.className}`}
        placeholder="Input your text..."
        value={props.value}
        onChange={onChange}
      />
    );
}
