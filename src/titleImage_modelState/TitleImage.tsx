import * as React from 'react';

import { Text } from '../text';
import { Spinner } from '../spinner';
import { Cropper, ICropInfo } from '../cropper';
import { Image } from '../image';

import './TitleImage.css';

const image = require('./image.jpg');

export interface ITitleImageState {
  mode: 'edit' | 'preview';
  isUploading: boolean;
  isCropping: boolean;
  text: string;
  cropInfo: ICropInfo;
}

export class TitleImage extends React.PureComponent<{}, ITitleImageState> {
  private static fullImage: ICropInfo = {
    top: 0,
    left: 0,
    ratio: 100,
  };

  private cropper: null | Cropper;

  public constructor(props: {}) {
    super(props);

    this.state = {
      mode: 'edit',
      isUploading: false,
      isCropping: false,
      text: 'Model State',
      cropInfo: TitleImage.fullImage,
    };
  }

  public render(): JSX.Element {
    return (
      <div>
        <div className="TitleImage-buttonBar">
          <button onClick={this.preview}>
            Preview
          </button>
          {
            this.state.mode === 'edit' &&
            <button onClick={this.uploadImage}>
              Upload Image
            </button>
          }
          {
            this.state.mode === 'edit' &&
            <button onClick={this.cropImage}>
              Crop Image
            </button>
          }
        </div>
        <div className="TitleImage-titleImage">
          <Image
            image={image}
            cropInfo={this.state.isCropping ? TitleImage.fullImage : this.state.cropInfo}
          />
          {
            // We also need to check edit mode here.
            this.state.isUploading &&
            !this.state.isCropping &&
            <Spinner
              className="TitleImage-imageSpinner"
            />
          }
          {
            // We also need to check edit mode here.
            this.state.isCropping &&
            <Cropper
              className="TitleImage-imageCropper"
              cropInfo={this.state.cropInfo}
              ref={cropper => this.cropper = cropper}
            />
          }
          {
            // We show the text is preview mode here.
            !this.state.isUploading &&
            !this.state.isCropping &&
            <Text
              className="TitleImage-text"
              preview={this.state.mode === 'preview'}
              value={this.state.text}
              onChange={this.updateText}
            />
          }
        </div>
      </div>
    );
  }

  private uploadImage = (): void => {
    // Hmm... How about the isCropping state when upload image?
    this.setState({
      isUploading: true,
      cropInfo: TitleImage.fullImage,
    });

    setTimeout(
      () => {
        // Suck! It needs to check if it is in preview mode after uploaded.
        this.setState({
          isUploading: false,
          isCropping: true,
        });
      },
      10000
    );
  }

  private cropImage = (): void => {
    this.setState((prevState) => (
      prevState.isCropping && this.cropper
      ? {
        isCropping: false,
        cropInfo: this.cropper.getCropInfo(),
      }
      : {
        isCropping: true,
        cropInfo: prevState.cropInfo,
      }
    ));
  }

  private updateText = (value: string): void => {
    this.setState({
      text: value,
    });
  }

  private preview = (): void => {
    this.setState((prevState) => ({
      mode: prevState.mode === 'edit' ? 'preview' : 'edit',
    }));
  }
}
