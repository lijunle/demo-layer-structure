import * as React from 'react';

import './Cropper.css';

export interface ICropInfo {
  top: number;
  left: number;
  ratio: number;
}

export interface ICropperProps {
  className?: string;
  cropInfo: ICropInfo;
}

export interface IDraggerInfo {
  element: 'dragger';
  layerPageX: number;
  layerPageY: number;
}

export interface ILayerInfo {
  element: 'layer';
  layerOffsetX: number;
  layerOffsetY: number;
  startEventOffsetX: number;
  startEventOffsetY: number;
}

export class Cropper extends React.PureComponent<ICropperProps> {
  private dragging: undefined | IDraggerInfo | ILayerInfo;
  private container: null | HTMLDivElement;
  private layer: null | HTMLDivElement;
  private dragger: null | HTMLDivElement;

  public componentDidMount(): void {
    document.addEventListener('mouseup', this.endDrag);
  }

  public componentWillUnmount(): void {
    document.removeEventListener('mouseup', this.endDrag);
  }

  public render(): JSX.Element {
    const layerStyle = {
      left: `${this.props.cropInfo.left}%`,
      top: `${this.props.cropInfo.top}%`,
      width: `${this.props.cropInfo.ratio}%`,
      height: `${this.props.cropInfo.ratio}%`,
    };

    return (
      <div
        className={`Cropper-container ${this.props.className}`}
        ref={container => this.container = container}
        onMouseDown={this.startDrag}
        onMouseMove={this.handleDrag}
      >
        <div
          className="Cropper-layer"
          style={layerStyle}
          ref={layer => this.layer = layer}
        >
          <div
            className="Cropper-dragger"
            ref={dragger => this.dragger = dragger}
          />
        </div>
      </div>
    );
  }

  public getCropInfo = (): ICropInfo => {
    if (this.layer) {
      return {
        top: parseFloat(this.layer.style.top || '0'),
        left: parseFloat(this.layer.style.left || '0'),
        ratio: parseFloat(this.layer.style.width || '100'),
      };
    } else {
      return {
        top: 0,
        left: 0,
        ratio: 100,
      };
    }
  }

  private startDrag = (event: React.MouseEvent<HTMLDivElement>) => {
    if (event.target === this.dragger && this.layer) {
      const { top, left } = this.layer.getBoundingClientRect();
      this.dragging = {
        element: 'dragger',
        layerPageX: window.pageXOffset + left,
        layerPageY: window.pageYOffset + top,
      };
    } else if (event.target === this.layer && this.layer) {
      this.dragging = {
        element: 'layer',
        layerOffsetX: this.layer.offsetLeft,
        layerOffsetY: this.layer.offsetTop,
        startEventOffsetX: event.pageX,
        startEventOffsetY: event.pageY,
      };
    }
  }

  private endDrag = () => {
    this.dragging = undefined;
  }

  private handleDrag = (event: React.MouseEvent<HTMLDivElement>) => {
    if (this.dragging && this.dragging.element === 'dragger') {
      this.handleDragDragger(event, this.dragging);
    } else if (this.dragging && this.dragging.element === 'layer') {
      this.handleDragLayer(event, this.dragging);
    }
  }

  private handleDragDragger = (event: React.MouseEvent<HTMLDivElement>, dragInfo: IDraggerInfo) => {
    if (this.container && this.layer) {
      // What is the relative position from the mouse to the container corner?
      const nextLeft = event.pageX - dragInfo.layerPageX;
      // const nextTop = event.pageY - dragInfo.layerPageY;

      // What is the new relative position of the dragger?
      const styleLeft = 100 * nextLeft / this.container.clientWidth;
      // const styleTop = 100 * nextTop / this.container.clientHeight;

      // Update the size of the layer.
      this.layer.style.width = `${styleLeft}%`;
      this.layer.style.height = `${styleLeft}%`;
    }
  }

  private handleDragLayer = (event: React.MouseEvent<HTMLDivElement>, dragInfo: ILayerInfo) => {
    if (this.container && this.layer) {
      // What is the offset of mouse moving?
      const offsetX = event.pageX - dragInfo.startEventOffsetX;
      const offsetY = event.pageY - dragInfo.startEventOffsetY;

      // The next left/top absolute position.
      const nextLeft = dragInfo.layerOffsetX + offsetX;
      const nextTop = dragInfo.layerOffsetY + offsetY;

      // The current layer width/height percentage.
      const layerWidth = parseFloat(this.layer.style.width || '0');
      const layerHeight = parseFloat(this.layer.style.height || '0');

      // The next left/top relative position.
      const styleLeft = Math.min(Math.max(0, 100 * nextLeft / this.container.clientWidth), 100 - layerWidth);
      const styleTop = Math.min(Math.max(0, 100 * nextTop / this.container.clientHeight), 100 - layerHeight);

      // Update the position of the layer.
      this.layer.style.left = `${styleLeft}%`;
      this.layer.style.top = `${styleTop}%`;
    }
  }
}
