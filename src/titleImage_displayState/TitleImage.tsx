import * as React from 'react';

import { Text } from '../text';
import { Spinner } from '../spinner';
import { Cropper, ICropInfo } from '../cropper';
import { Image } from '../image';

import './TitleImage.css';

const image = require('./image.jpg');

export interface ITitleImageState {
  showButtons: boolean;
  showUploadSpinner: boolean;
  showCropper: boolean;
  showText: 'none' | 'edit' | 'preview';
  text: string;
  cropInfo: ICropInfo;
}

export class TitleImage extends React.PureComponent<{}, ITitleImageState> {
  private static fullImage: ICropInfo = {
    top: 0,
    left: 0,
    ratio: 100,
  };

  private cropper: null | Cropper;

  public constructor(props: {}) {
    super(props);

    this.state = {
      showButtons: true,
      showUploadSpinner: false,
      showCropper: false,
      showText: 'edit',
      text: 'Display State',
      cropInfo: TitleImage.fullImage,
    };
  }

  public render(): JSX.Element {
    return (
      <div>
        <div className="TitleImage-buttonBar">
          <button onClick={this.preview}>
            Preview
          </button>
          {
            this.state.showButtons &&
            <button onClick={this.uploadImage}>
              Upload Image
            </button>
          }
          {
            this.state.showButtons &&
            <button onClick={this.cropImage}>
              Crop Image
            </button>
          }
        </div>
        <div className="TitleImage-titleImage">
          <Image
            image={image}
            cropInfo={this.state.cropInfo}
          />
          {
            this.state.showUploadSpinner &&
            <Spinner
              className="TitleImage-imageSpinner"
            />
          }
          {
            this.state.showCropper &&
            <Cropper
              className="TitleImage-imageCropper"
              cropInfo={this.state.cropInfo}
              ref={cropper => this.cropper = cropper}
            />
          }
          {
            this.state.showText !== 'none' &&
            <Text
              className="TitleImage-text"
              preview={this.state.showText === 'preview'}
              value={this.state.text}
              onChange={this.updateText}
            />
          }
        </div>
      </div>
    );
  }

  private uploadImage = (): void => {
    this.setState({
      showUploadSpinner: true,
      showCropper: false,
      showText: 'none',
      cropInfo: TitleImage.fullImage,
    });

    setTimeout(
      () => {
        this.setState({
          showUploadSpinner: false,
          showCropper: true,
          showText: 'none',
          cropInfo: TitleImage.fullImage,
        });
      },
      3000
    );
  }

  private cropImage = (): void => {
    this.setState((prevState) => ({
      showCropper: !prevState.showCropper,
      showText: 'none',
      cropInfo: TitleImage.fullImage, // Suck! It lost the exiting crop info.
    }));

    if (this.cropper) {
      this.setState({
        showText: 'edit', // Really? How about the image is still uploading?
        cropInfo: this.cropper.getCropInfo(),
      });
    }
  }

  private updateText = (value: string): void => {
    this.setState({
      text: value,
    });
  }

  private preview = (): void => {
    this.setState((prevState) => (
      prevState.showButtons // Previously, it is in edit mode.
      ? {
        showButtons: false,
        showCropper: false,
        showText: 'preview',
        showUploadSpinner: false,
      }
      : {
        showButtons: true,
        showCropper: true,
        showText: 'edit', // Really?
        showUploadSpinner: false // Suck! I want to check if the image is uploading.
      }
    ));
  }
}
