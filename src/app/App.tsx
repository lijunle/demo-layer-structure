import * as React from 'react';

// The component state maps to what is displaying on the screen.
// import { TitleImage } from '../titleImage_displayState';

// The component state maps to the data model of the control.
// import { TitleImage } from '../titleImage_modelState';

// Separate the data model, handle state layer by layer, use primitive component.
import { TitleImage } from '../titleImage_layerStructure';

import './App.css';

export class App extends React.PureComponent {
  render() {
    return (
      <div>
        <header className="App-header">
          <h1 className="App-title">Layer Structure</h1>
          <h3 className="App-author">Junle Li (junlel@microsoft.com)</h3>
        </header>
        <div className="App-body">
          <TitleImage />
          <section>
            <p>Features:</p>
            <ul>
              <li>There is preview mode and edit mode for the control.</li>
              <li>In preview mode, it shows the image title and the cropped background image.</li>
              <li>In edit mode, it shows the the buttons to manipulate the background image.</li>
              <li>Use the "Upload Image" button to upload the image. After uploaded, turn to crop view.</li>
              <li>Use the "Crop Image" button to start/finish cropping image.</li>
              <li>Use the title input to fill the title for the image.</li>
              <li>Use the "Preview" button to switch between edit mode and preview mode.</li>
            </ul>
            <p>Hard parts:</p>
            <ul>
              <li>
                During the image is uploading, the user can start cropping by manually click the "Crop Image" button.
                After the image is uploaded, it turns to the crop view with user cropped size during uploading.
              </li>
              <li>
                During the image is uploading, the user can switch to preview mode. If the image finish uploading in
                preview mode, do not turn to crop view.
              </li>
              <li>
                During the image is uploading, the user can switch to preview/edit mode back and forth. The image shows
                the "Uploading" sign correctly and turn to crop view according to the current view.
              </li>
              <li>
                During the image is cropping, the user can click "Preview" button to switch to preview mode.
                If click again to switch back, the image still shows the crop view for continue cropping.
              </li>
            </ul>
          </section>
        </div>
      </div>
    );
  }
}
