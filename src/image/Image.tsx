import * as React from 'react';

import { ICropInfo } from '../cropper';

import './Image.css';

export interface IImageProps {
  image: string;
  cropInfo: ICropInfo;
}

export function Image(props: IImageProps): JSX.Element {
  const transformSteps = [
    `scale(${100 / props.cropInfo.ratio})`,
    `translateX(${50 - props.cropInfo.left - 0.5 * props.cropInfo.ratio}%)`,
    `translateY(${50 - props.cropInfo.top - 0.5 * props.cropInfo.ratio}%)`,
  ];

  return (
    <div
      className="Image-container"
    >
      <img
        className="Image-image"
        style={{ transform: transformSteps.join(' ') }}
        src={props.image}
      />
    </div>
  );
}
