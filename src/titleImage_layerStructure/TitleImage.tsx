import * as React from 'react';

import { Text } from '../text';
import { Spinner } from '../spinner';
import { Cropper, ICropInfo } from '../cropper';
import { Image } from '../image';
import { ITitleImageController } from './TitleImage.types';

import './TitleImage.css';

const image = require('./image.jpg');

export interface ITitleImageProps {
  showButtons: boolean;
  showUploadSpinner: boolean;
  showCropper: false | ICropInfo;
  showText: 'none' | 'edit' | 'preview';
  text: string;
  backgroundImage: ICropInfo;
  controller: ITitleImageController;
}

export class TitleImage extends React.PureComponent<ITitleImageProps> {
  public cropper: null | Cropper;

  public render(): JSX.Element {
    return (
      <div>
        <div className="TitleImage-buttonBar">
          <button onClick={this.props.controller.togglePreviewMode}>
            Preview
          </button>
          {
            this.props.showButtons &&
            <button onClick={this.props.controller.uploadImage}>
              Upload Image
            </button>
          }
          {
            this.props.showButtons &&
            <button onClick={this.props.controller.toggleCropMode}>
              Crop Image
            </button>
          }
        </div>
        <div className="TitleImage-titleImage">
          <Image
            image={image}
            cropInfo={this.props.backgroundImage}
          />
          {
            this.props.showUploadSpinner &&
            <Spinner
              className="TitleImage-imageSpinner"
            />
          }
          {
            this.props.showCropper &&
            <Cropper
              className="TitleImage-imageCropper"
              cropInfo={this.props.showCropper}
              ref={cropper => this.cropper = cropper}
            />
          }
          {
            this.props.showText !== 'none' &&
            <Text
              className="TitleImage-text"
              preview={this.props.showText === 'preview'}
              value={this.props.text}
              onChange={this.props.controller.changeText}
            />
          }
        </div>
      </div>
    );
  }
}
