import * as React from 'react';

import { ITitleImageModel, ITitleImageController } from './TitleImage.types';
import { TitleImage } from './TitleImage';

export interface IUploadLayerProps extends ITitleImageModel {
  controller: ITitleImageController;
}

export function UploadLayer(props: IUploadLayerProps): JSX.Element {
  if (props.isUploading) {
    return (
      <TitleImage
        showButtons={true}
        showCropper={false}
        showUploadSpinner={true}
        showText="none"
        text={props.text}
        backgroundImage={props.cropInfo}
        controller={props.controller}
      />
    );
  } else {
    return (
      <TitleImage
        showButtons={true}
        showCropper={false}
        showUploadSpinner={false}
        showText="edit"
        text={props.text}
        backgroundImage={props.cropInfo}
        controller={props.controller}
      />
    );
  }
}
