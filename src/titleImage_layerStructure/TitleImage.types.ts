import { ICropInfo } from '../cropper';

export interface ITitleImageModel {
  mode: 'edit' | 'preview';
  isUploading: boolean;
  isCropping: boolean;
  text: string;
  cropInfo: ICropInfo;
}

export interface ITitleImageController {
  togglePreviewMode(): void;
  uploadImage(): void;
  toggleCropMode(): void;
  changeText(text: string): void;
}
