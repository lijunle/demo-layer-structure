import * as React from 'react';

import { ITitleImageController, ITitleImageModel } from './TitleImage.types';
import { TitleImage } from './TitleImage';
import { ICropInfo } from '../cropper';
import { UploadLayer } from './UploadLayer';

export interface ICropLayerProps extends ITitleImageModel {
  controller: ITitleImageController;
}

export class CropLayer extends React.PureComponent<ICropLayerProps> {
  public static fullImage: ICropInfo = {
    top: 0,
    left: 0,
    ratio: 100,
  };

  private static titleImageMap: WeakMap<ITitleImageController, null | TitleImage> =
    new WeakMap<ITitleImageController, null | TitleImage>();

  public static getCropInfo(controller: ITitleImageController): ICropInfo {
    const titleImage = CropLayer.titleImageMap.get(controller);
    if (titleImage && titleImage.cropper) {
      return titleImage.cropper.getCropInfo();
    } else {
      return CropLayer.fullImage;
    }
  }

  public render(): JSX.Element {
    if (this.props.isCropping) {
      return (
        <TitleImage
          showButtons={true}
          showCropper={this.props.cropInfo}
          showUploadSpinner={false}
          text={this.props.text}
          showText="none"
          backgroundImage={CropLayer.fullImage}
          controller={this.props.controller}
          ref={this.refTitleImage}
        />
      );
    } else {
      return (
        <UploadLayer
          {...this.props}
        />
      );
    }
  }

  private refTitleImage = (titleImage: null | TitleImage) => {
    CropLayer.titleImageMap.set(this.props.controller, titleImage);
  }
}
