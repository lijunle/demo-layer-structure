import * as React from 'react';

import { ITitleImageModel, ITitleImageController } from './TitleImage.types';
import { TitleImage } from './TitleImage';
import { CropLayer } from './CropLayer';

export interface IPreviewLayerProps extends ITitleImageModel {
  controller: ITitleImageController;
}

export function PreviewLayer(props: IPreviewLayerProps): JSX.Element {
  if (props.mode === 'preview') {
    return (
      <TitleImage
        showButtons={false}
        showUploadSpinner={false}
        showCropper={false}
        text={props.text}
        showText="preview"
        backgroundImage={props.cropInfo}
        controller={props.controller}
      />
    );
  } else {
    return (
      <CropLayer
        {...props}
      />
    );
  }
}