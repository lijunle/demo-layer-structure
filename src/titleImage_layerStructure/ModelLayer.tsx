import * as React from 'react';

import { ITitleImageModel, ITitleImageController } from './TitleImage.types';
import { PreviewLayer } from './PreviewLayer';
import { CropLayer } from './CropLayer';

export type IModelLayerState = ITitleImageModel;

export class ModelLayer extends React.PureComponent<{}, IModelLayerState> {
  private readonly controller: ITitleImageController;

  private uploadTimer: NodeJS.Timer;

  public constructor(props: {}) {
    super(props);

    this.state = {
      mode: 'edit',
      isUploading: false,
      isCropping: false,
      text: 'Layer Structure',
      cropInfo: CropLayer.fullImage,
    };

    this.controller = {
      togglePreviewMode: this.togglePreviewMode,
      toggleCropMode: this.toggleCropMode,
      uploadImage: this.uploadImage,
      changeText: this.changeText,
    };
  }

  public render(): JSX.Element {
    return (
      <PreviewLayer
        {...this.state}
        controller={this.controller}
      />
    );
  }

  private uploadImage = (): void => {
    this.setState({
      isUploading: true,
      isCropping: false,
      cropInfo: CropLayer.fullImage,
    });

    clearTimeout(this.uploadTimer);
    this.uploadTimer = setTimeout(
      () => {
        this.setState({
          isUploading: false,
          isCropping: true,
        });
      },
      20000
    );
  }

  private toggleCropMode = (): void => {
    this.setState((prevState) => (
      prevState.isCropping
      ? {
        isCropping: false,
        cropInfo: CropLayer.getCropInfo(this.controller),
      }
      : {
        isCropping: true,
        cropInfo: prevState.cropInfo,
      }
    ));
  }

  private changeText = (value: string): void => {
    this.setState({
      text: value,
    });
  }

  private togglePreviewMode = (): void => {
    this.setState((prevState) => ({
      mode: prevState.mode === 'edit' ? 'preview' : 'edit',
    }));
  }
}
