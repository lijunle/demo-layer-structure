import * as React from 'react';

import './Spinner.css';

export interface ISpinnerProps {
  className?: string;
}

export function Spinner(props: ISpinnerProps): JSX.Element {
  return (
    <div className={`Spinner-overlay ${props.className}`}>
      <p className="Spinner-text">Uploading...</p>
    </div>
  );
}
